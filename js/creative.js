  (function($) {
    $.fn.CustomMap = function(options) {

      var posLatitude = $('#map').data('position-latitude'),
        posLongitude = $('#map').data('position-longitude');

      var settings = $.extend({
        home: {
          latitude: posLatitude,
          longitude: posLongitude
        },
        text: '<div class="map-popup"><h4>Web Development | ZoOm-Arts</h4><p>A web development blog for all your HTML5 and WordPress needs.</p></div>',
        icon_url: $('#map').data('marker-img'),
        zoom: 15
      }, options);

      var coords = new google.maps.LatLng(settings.home.latitude, settings.home.longitude);

      return this.each(function() {
        var element = $(this);

        var options = {
          zoom: settings.zoom,
          center: coords
        };

        var map = new google.maps.Map(element[0], options);

        var icon = {
          url: settings.icon_url,
          origin: new google.maps.Point(0, 0)
        };

        var marker = new google.maps.Marker({
          position: coords,
          map: map,
          icon: icon,
          draggable: false
        });
      });

    };
  }(jQuery));



(function($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $(document).on('click', 'a.page-scroll', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    });

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })

    // Initialize and Configure Scroll Reveal Animation
    window.sr = ScrollReveal();
    sr.reveal('.sr-icons', {
        duration: 600,
        scale: 0.3,
        distance: '0px'
    }, 200);
    sr.reveal('.sr-button', {
        duration: 1000,
        delay: 200
    });
    sr.reveal('.sr-contact', {
        duration: 600,
        scale: 0.3,
        distance: '0px'
    }, 300);

    // Initialize and Configure Magnific Popup Lightbox Plugin
    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
        }
    });

    $("#gallery").unitegallery({
        lightbox_type: "compact",
        tile_enable_icons:false,
        tile_enable_textpanel:true,
        tile_textpanel_title_text_align: "center",
        tile_textpanel_always_on:true,
        tile_textpanel_source:"desc",
        lightbox_textpanel_enable_description: true,
    });
    jQuery('#map').CustomMap();

})(jQuery); // End of use strict

function initMap() {
var mapOptions = {
    center: new google.maps.LatLng(51.5, 50.12)
}
var map = new google.maps.Map(document.getElementById("map-container"), mapOptions);
}

